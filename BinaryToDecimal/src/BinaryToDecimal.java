
public class BinaryToDecimal
{
	
	public int convertBinaryToDecimal(int number)
	{
		int binary = number;
		int decimal = 0;
		int base;
		int power = 0;
		
		while (binary>0)
		{
			base = binary%10; // takes the last digit of binary number
			decimal+= base*Math.pow(2, power); // last digit * 2 to the power starting at 0++
			power++;//increment power each iteration
			binary = binary/10;//chop off last digit of binary number
		}
		
		return decimal;
	}
}
