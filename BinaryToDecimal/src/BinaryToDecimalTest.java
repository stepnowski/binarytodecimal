import java.util.Scanner;

public class BinaryToDecimalTest
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int binary;
		int decimal;
		
		BinaryToDecimal convert = new BinaryToDecimal();
		
		System.out.print("Please enter a binary number:");
		
		binary = input.nextInt();
		decimal = convert.convertBinaryToDecimal(binary);
		
		System.out.printf("the output is: %d", decimal);
	}

}
